# Pipeline-testdata Repository

## Git Setup

While no specific changes are required to your Git setup, there are some changes which make the checkout of the data repository much more convenient because without these changes the default credential caching timeout will cause Git LFS to prompt (often) as files are downloaded.

### OSX

For OSX, setting the OSX keychain as the credential source allows the LFS checkout to proceed without prompting for passwords. This can be done with:

~~~~
-bash-4.1$ git config --global credential.helper osxkeychain
~~~~

### Linux

For Linux, there is no facility for general password management so the easiest solution for Linux is to increase the credential timeout:

~~~~
-bash-4.1$ git config --global credential.helper cache
-bash-4.1$ git config --global credential.helper 'cache --timeout=3600'
~~~~

## Git LFS Setup

Git LFS is distributed as an add-on to Git, so before you begin to use Git Lfs, ensure that it is actually installed. If you run (and see) the following:

~~~~
-bash-4.1$ git lfs help
No manual entry for gitlfs
-bash-4.1$
~~~~

It means that Git LFS is not installed on your system, contact your local system administrator. Most CASA Linux developers should get Git LFS as part of the installation of 'casa-toolset-2' which includes Git LFS.
Setup Your Git LFS Environment

LFS is switched on or off by Git users not by something committed to the repository. For this reason, you should add LFS to your Git environment on any logins that you will use to commit changes to the CASA Data Repository. It is best to set LFS as a global option so tha you do not need to initialize LFS each time you clone the data repository. You can do this by running the following commands at the bash command line:

~~~~
git config --global filter.lfs.required true
git config --global filter.lfs.clean "git-lfs clean -- %f"
git config --global filter.lfs.smudge "git-lfs smudge -- %f"
git config --global filter.lfs.process "git-lfs filter-process"
~~~~

It is also possible to set up Git LFS on a per-repository basis.


## Checking Out the Pipeline Data Repository

~~~~
git clone https://open-bitbucket.nrao.edu/scm/pipe/pipeline-testdata.git
~~~~
Git output should indicate file sizes in megabytes. If it does not, the LFS filter setting are likely not correct. See above for the initial setup.


## Committing Changes

Remember to run "git pull" prior to beginning to make changes.

To do this, just check the new files (or replacement files) into place, and then add them as normal from the root of your Git clone.  However, it is important to check to ensure that the change registers as expected as we go through the commit. At this point, Git will see the new file:

~~~~
-bash-4.2$ git status -s
?? pl-unittest/3DDAT.fits
-bash-4.2$
~~~~

but LFS will not:

~~~~
-bash-4.2$ git lfs status --porcelain
-bash-4.2$
~~~~

IMPORTANT: The top level of the repository is not managed by LFS. If you need to add another directory on the top level, you must first add it to the .gitattributes file. To do this, execute the command:

~~~~
git lfs track "myfolder/**"
~~~~

Verify that the contents match the existing directories and then commit the .gitattributes file to the repository. Then proceed with adding new files as described below. You will also need to check-in the new version of .gitattributes

Next add the new file from the root of your data repository clone:

~~~~
-bash-4.2$ git add pl-unittest/3DDAT.fits
-bash-4.2$
~~~~

At this point, both Git and Git LFS should recogize the new file for being committed:

~~~~
-bash-4.2$ git status -s
A  pl-unittest/3DDAT.fits
-bash-4.2$
-bash-4.2$ git lfs status --porcelain
A  pl-unittest/3DDAT.fits 10137600
-bash-4.2$
~~~~

If you do not see your changes reflected in the output from "lfs status", do not commit your changes because commit files reported by "git status" but not reported by "git lfs status" will result in binary data being committed directly to Git (as binary files) instead of through Git LFS.

With our changes visible to both Git and Git LFS, it is safe to commit them:

~~~~
-bash-4.2$ git commit -m 'changes which should not be pushed'
[main 93cc524] changes which should not be pushed
1 file changed, 3 insertions(+)
create mode 100644 pl-unittest/3DDAT.fits
-bash-4.2$ 
~~~~

The "changes which should not be pushed" comment simply refers to the fact that we've just committed a bogus file to our local repository which we do not want to be pushed into the bitbucket repository. With a normal commit to the data repository, with files which should be shared, it would now be safe to push these files up to the server.

When deleting files from the data repository, the deletions will not be listed in the "git lfs status --porcelain" output. This is because when deleting files the large binary files not deleted because they are required when checking out older revisions of the data repository.

## Check Before Committing

It is very important to check the status of your data repository clone before doing a commit of changed files to your local repository. Failure to do this (even should you be on a non-main branch), could lead to the need to reconstitute the CASA Data Repository on the server from scratch.

This step is simple. As described in the "Committing Changes" section, all you need to do is compare the output of:

~~~~
git status -s
~~~~
and
~~~~
git lfs status --porcelain
~~~~
to ensure that each reports knowledge of the files that are about to be committed. In our example above, the interaction looked like:
~~~~
-bash-4.2$ git status -s
A  pl-unittest/3DDAT.fits
-bash-4.2$
-bash-4.2$ git lfs status --porcelain
A  pl-unittest/3DDAT.fits 10137600
-bash-4.2$
~~~~
When deleting files from the data repository, the deletions will not be listed in the "git lfs status --porcelain" output.
Check Before Pushing Upstream

Double check that your files are managed by LFS. One way to do this is to use LFS ls-files. For example:

~~~~
git lfs ls-files | grep stakeholders/alma/E2E6.1.00034.S_tclean.ms/SYSPOWER/table.dat
~~~~
Another, and perhaps more robust verification is to compare the file size in Git to the actual file size on disk.

In this example the file size on disk is 2283 bytes but the size reported by Git is only 129 bytes. This means that the binary is indeed managed by LFS.

~~~~
ls -l  stakeholders/alma/E2E6.1.00020.S_tclean.ms/ASDM_RECEIVER/table.dat

-rw-r--r-- 1 username group 2283 Mar  4 15:09 stakeholders/alma/E2E6.1.00020.S_tclean.ms/ASDM_RECEIVER/table.dat

git ls-tree main -rl | grep  stakeholders/alma/E2E6.1.00020.S_tclean.ms/ASDM_RECEIVER/table.dat

100644 blob c995547dd417f4def10d38d969fe94a6aff9563d     129    stakeholders/alma/E2E6.1.00020.S_tclean.ms/ASDM_RECEIVER/table.dat
~~~~ 

Referencing pipeline-testdata in Unit Tests

Add the following to your  ~/.casa/config.py

~~~~
datapath = [ '/home/<user>/casa-data', '/home/<user>/pipeline-testdata' ]
~~~~
Replace the sample paths with your actual data paths.

Use casatools.ctsys utility to refer to the actual data. This will return the full path if data is found.

~~~~
casatools.ctsys.resolve("pl-unittest/VLASS1.1.ql.T19t20.J155950+333000.fits")
~~~~

